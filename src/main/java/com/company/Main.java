package com.company;

import java.util.Date;
import java.util.Scanner;

public class Main {
    public static int punkty;

    public static class Produkty {
        public int Mleko;
        public int WodaMineralna;
        public int WodaZrodlana;
        public int Jajko;
        public int Chleb;
        public int Czipsy;
        public int Czekolada;
        public int Baton;
        public int SerekWiejski;
        public int Smietana;
        public float Kurczak;
        public float Wolowina;
        public float Wieprzowina;
        public float Papryka1kg;
        public float Cebula1kg;
        public float Szczypior;

        public Produkty() {
        }

        public Produkty(int mleko, int wodaMineralna, int wodaZrodlana, int jajko, int chleb, int czipsy,
                        int czekolada, int baton, int serekWiejski, int smietana, float kurczak, float wolowina,
                        float wieprzowina, float papryka1kg, float cebula1kg, float szczypior) {
            Mleko = mleko;
            WodaMineralna = wodaMineralna;
            WodaZrodlana = wodaZrodlana;
            Jajko = jajko;
            Chleb = chleb;
            Czipsy = czipsy;
            Czekolada = czekolada;
            Baton = baton;
            SerekWiejski = serekWiejski;
            Smietana = smietana;
            Kurczak = kurczak;
            Wolowina = wolowina;
            Wieprzowina = wieprzowina;
            Papryka1kg = papryka1kg;
            Cebula1kg = cebula1kg;
            Szczypior = szczypior;
        }
        public int sprawdzUjemne(int produkt) {
            return produkt;
        }
        public int getMleko() {
            return Mleko;
        }

        public void setMleko(int mleko) {
            Mleko = mleko;
        }

        public int getWodaMineralna() {
            return WodaMineralna;
        }

        public void setWodaMineralna(int wodaMineralna) {
            WodaMineralna = wodaMineralna;
        }

        public int getWodaZrodlana() {
            return WodaZrodlana;
        }

        public void setWodaZrodlana(int wodaZrodlana) {
            WodaZrodlana = wodaZrodlana;
        }

        public int getJajko() {
            return Jajko;
        }

        public void setJajko(int jajko) {
            Jajko = jajko;
        }

        public int getChleb() {
            return Chleb;
        }

        public void setChleb(int chleb) {
            Chleb = chleb;
        }

        public int getCzipsy() {
            return Czipsy;
        }

        public void setCzipsy(int czipsy) {
            Czipsy = czipsy;
        }

        public int getCzekolada() {
            return Czekolada;
        }

        public void setCzekolada(int czekolada) {
            Czekolada = czekolada;
        }

        public int getBaton() {
            return Baton;
        }

        public void setBaton(int baton) {
            Baton = baton;
        }

        public int getSerekWiejski() {
            return SerekWiejski;
        }

        public void setSerekWiejski(int serekWiejski) {
            SerekWiejski = serekWiejski;
        }

        public int getSmietana() {
            return Smietana;
        }

        public void setSmietana(int smietana) {
            Smietana = smietana;
        }

        public float getKurczak() {
            return Kurczak;
        }

        public void setKurczak(float kurczak) {
            Kurczak = kurczak;
        }

        public float getWolowina() {
            return Wolowina;
        }

        public void setWolowina(float wolowina) {
            Wolowina = wolowina;
        }

        public float getWieprzowina() {
            return Wieprzowina;
        }

        public void setWieprzowina(float wieprzowina) {
            Wieprzowina = wieprzowina;
        }

        public float getPapryka1kg() {
            return Papryka1kg;
        }

        public void setPapryka1kg(float papryka1kg) {
            Papryka1kg = papryka1kg;
        }

        public float getCebula1kg() {
            return Cebula1kg;
        }

        public void setCebula1kg(float cebula1kg) {
            Cebula1kg = cebula1kg;
        }

        public float getSzczypior() {
            return Szczypior;
        }

        public void setSzczypior(float szczypior) {
            Szczypior = szczypior;
        }

        @Override
        public String toString() {
            return "Twoje produkty\n" +
                    "Mleko=" + Mleko + "\n" +
                    "WodaMineralna=" + WodaMineralna + "\n" +
                    "WodaZrodlana=" + WodaZrodlana + "\n" +
                    "Jajko=" + Jajko + "\n" +
                    "Chleb=" + Chleb + "\n" +
                    "Kurczak=" + Kurczak + "\n" +
                    "Wolowina=" + Wolowina + "\n" +
                    "Wieprzowina=" + Wieprzowina + "\n" +
                    "SerekWiejski=" + SerekWiejski + "\n" +
                    "Smietana=" + Smietana + "\n" +
                    "Papryka1kg=" + Papryka1kg + "\n" +
                    "Cebula1kg=" + Cebula1kg + "\n" +
                    "Szczypior=" + Szczypior + "\n" +
                    "Czipsy=" + Czipsy + "\n" +
                    "Czekolada=" + Czekolada + "\n" +
                    "Baton=" + Baton;
        }
    }
    public interface Cena{
        float MlekoCena = 2f;
        float WodaMineralnaCena = 2f;
        float WodaZrodlanaCena = 1.5f;
        float JajkoCena = 0.7f;
        float ChlebCena = 3f;
        float CzipsyCena = 5f;
        float CzekoladaCena = 4f;
        float BatonCena = 2.5f;
        float SerekWiejskiCena = 2f;
        float SmietanaCena = 2.3f;
        float KurczakCena = 9f;
        float WolowinaCena = 15f;
        float WieprzowinaCena = 12f;
        float Papryka1kgCena = 5.5f;
        float Cebula1kgCena = 3.5f;
        float SzczypiorCena = 3f;


    }
    public static class Zakupy extends Produkty implements Cena{
        String Hello = "Witaj w zakupach online!";
        public Zakupy() {
        }

        public Zakupy(int mleko, int wodaMineralna, int wodaZrodlana, int jajko, int chleb, int czipsy,
                      int czekolada, int baton, int serekWiejski, int smietana, float kurczak, float wolowina,
                      float wieprzowina, float papryka1kg, float cebula1kg, float szczypior) {
            super(mleko, wodaMineralna, wodaZrodlana, jajko, chleb, czipsy, czekolada, baton,
                    serekWiejski, smietana, kurczak, wolowina, wieprzowina, papryka1kg, cebula1kg, szczypior);
        }

        public void Koszyk(){
            System.out.println(Hello);
            Scanner scanner = new Scanner(System.in);

            System.out.println("Wpisz ilość mlek ");
            int Mleko_x= scanner.nextInt();
            setMleko(Mleko_x);

            System.out.println("Wpisz ilość wody mineralnej ");
            int WodaMineralna_x= scanner.nextInt();
            setWodaMineralna(WodaMineralna_x);

            System.out.println("Wpisz ilość wody źródlanej ");
            int WodaZrodlana_x= scanner.nextInt();
            setWodaZrodlana(WodaZrodlana_x);

            System.out.println("Wpisz ilość jajek ");
            int Jajko_x= scanner.nextInt();
            setJajko(Jajko_x);

            System.out.println("Wpisz ilość chlebów ");
            int Chleb_x= scanner.nextInt();
            setChleb(Chleb_x);

            System.out.println("Wpisz ilość kurczaka");
            float Kurczak_x= scanner.nextFloat();
            setKurczak(Kurczak_x);

            System.out.println("Wpisz ilość wolowiny ");
            float Wolowina_x= scanner.nextFloat();
            setWolowina(Wolowina_x);

            System.out.println("Wpisz ilość wieprzowiny ");
            float Wieprzowina_x= scanner.nextFloat();
            setWieprzowina(Wieprzowina_x);

            System.out.println("Wpisz ilość serków wiejskich ");
            int SerekWiejski_x= scanner.nextInt();
            setSerekWiejski(SerekWiejski_x);

            System.out.println("Wpisz ilość śmietan ");
            int Smietana_x= scanner.nextInt();
            setSmietana(Smietana_x);

            System.out.println("Wpisz wagę papryki w kg ");
            float Papryka1kg_x= scanner.nextFloat();
            setPapryka1kg(Papryka1kg_x);

            System.out.println("Wpisz wagę cebuli w kg ");
            float Cebula1kg_x= scanner.nextFloat();
            setCebula1kg(Cebula1kg_x);

            System.out.println("Wpisz ilość szczypioru ");
            float Szczypior_x= scanner.nextFloat();
            setSzczypior(Szczypior_x);

            System.out.println("Wpisz ilość paczek czipsów ");
            int Czipsy_x= scanner.nextInt();
            setCzipsy(Czipsy_x);

            System.out.println("Wpisz ilość czekolad ");
            int Czekolada_x= scanner.nextInt();
            setCzekolada(Czekolada_x);

            System.out.println("Wpisz ilość Batonów ");
            int Baton_x= scanner.nextInt();
            setBaton(Baton_x);
        }

        public float Paragon(){
            float DoZaplaty = getMleko() * Cena.MlekoCena + getWodaMineralna() * Cena.WodaMineralnaCena + getWodaZrodlana() * Cena.WodaZrodlanaCena
                    + getJajko() * Cena.JajkoCena + getChleb() * Cena.ChlebCena + getKurczak() * Cena.KurczakCena + getWolowina() * Cena.WolowinaCena
                    + getWieprzowina() * Cena.WieprzowinaCena + getSerekWiejski() * Cena.SerekWiejskiCena + getSmietana() * Cena.SmietanaCena
                    + getPapryka1kg() * Cena.Papryka1kgCena + getCebula1kg() * Cena.Cebula1kgCena + getSzczypior() * Cena.SzczypiorCena
                    + getCzipsy() * Cena.CzipsyCena + getCzekolada() * Cena.CzekoladaCena + getBaton() * Cena.BatonCena;
            return (Math.round(DoZaplaty * 100.0f) / 100.0f);
        }
    }
    public static class Punkty{
        public Punkty() {
        }

        public int LiczPunkty(Zakupy zakupy){
            punkty += zakupy.Paragon() / 20;
            return punkty;
        }

        public void WymienPunkty(Zakupy zakupy){
            if (punkty <= 5 ) {
                System.out.println("dostajesz czekoladę za zdobyte punkty");
                zakupy.Czekolada ++;
            }
            else if (punkty <= 10){
                System.out.println("dostajesz wodę mineralną x4 za zdobyte punkty");
                zakupy.WodaMineralna += 4;
            }
            else if(punkty <= 20){
                System.out.println("dostajesz serek wiejski x2, śmietanę x2, mleko x2 za zdobyte punkty");
                zakupy.SerekWiejski += 2;
                zakupy.Smietana += 2;
                zakupy.Mleko +=2;
            }
            else{
                System.out.println("dostajesz 3kg wołowiny za zdobyte punkty");
                zakupy.Wolowina +=3;
            }
        }

        public int getPunkt(){
            return punkty;
        }
    }

    public static void main(String[] args) {
        Zakupy a = new Zakupy(1,1,1,1,1,1,1,
                1,1,1,1,1,1,1,1,1);
        Punkty p = new Punkty();
        a.Koszyk();
        System.out.println("#### PODSUMOWANIE ####");
        Date nowDate = new Date();
        System.out.println("Data sprzedaży: " + nowDate);
        System.out.println("Zdobyte punkty = " + p.LiczPunkty(a));
        p.WymienPunkty(a);
        System.out.println(a);
        System.out.println("Suma = " + a.Paragon());

    }
}
