package com.company;


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


import static org.junit.Assert.*;


public class MainTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void Paragon_should_return_actual_cost_of_all_products() {
        // given
        Main.Zakupy zakupy = new Main.Zakupy(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);

        // when
        int result = (int)zakupy.Paragon();

        // then
        assertEquals(result, 73);
    }

    @Test
    public void Serek_kurczak_and_chleb_return_spefic_value() {
        // given
        Main.Zakupy zakupy = new Main.Zakupy(0,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0);

        // when
        int result = (int)zakupy.Paragon();

        // then
        assertEquals(result, 14);
    }
    @Test
    public void Check_the_scored_points() {
        // given
        Main.Punkty pkt = new Main.Punkty();
        String res1 = "dostajesz czekoladę za zdobyte punkty";
        String res2 = "dostajesz wodę mineralną x4 za zdobyte punkty";
        String res3 = "dostajesz serek wiejski x2, śmietanę x2, mleko x2 za zdobyte punkty";
        String res4 = "dostajesz 3kg wołowiny za zdobyte punkty";

        // when
        int result = pkt.getPunkt();

        // then
        if (result <= 5 ) {
            assertEquals(res1,"dostajesz czekoladę za zdobyte punkty");
        }
        else if (result <= 10){
            assertEquals(res2,"dostajesz wodę mineralną x4 za zdobyte punkty");
        }
        else if(result <= 20){
            assertEquals(res3,"dostajesz serek wiejski x2, śmietanę x2, mleko x2 za zdobyte punkty");
        }
        else{
            assertEquals(res4,"dostajesz 3kg wołowiny za zdobyte punkty");
        }
    }



    @Test
    public void Price_should_return_positive_value_for_each_product() {
        // given
        float c0 = Main.Cena.MlekoCena;
        float c1 = Main.Cena.WodaMineralnaCena;
        float c2 = Main.Cena.WodaZrodlanaCena;
        float c3 = Main.Cena.JajkoCena;
        float c4 = Main.Cena.ChlebCena;
        float c5 = Main.Cena.CzipsyCena;
        float c6 = Main.Cena.CzekoladaCena;
        float c7 = Main.Cena.BatonCena;
        float c8 = Main.Cena.SerekWiejskiCena;
        float c9 = Main.Cena.SmietanaCena;
        float c10 = Main.Cena.KurczakCena;
        float c11 = Main.Cena.WolowinaCena;
        float c12 = Main.Cena.WieprzowinaCena;
        float c13 = Main.Cena.Papryka1kgCena;
        float c14 = Main.Cena.Cebula1kgCena;
        float c15 = Main.Cena.SzczypiorCena;

        // when
        boolean result = true;
        if (c0 < 0 || c1 <0 || c2 <0 || c3 <0 || c4 <0 || c5 <0 || c5 <0 || c6 <0 || c7 <0 || c8 <0
                || c9 <0 || c10 <0 || c11 <0 || c12 <0 || c13 <0 || c14 <0 || c15 <0){
            assertFalse(result);
        }else {
            // then
            assertTrue(result);
        }
    }

    @Test
    public void Zakupy_return_specific_greetings(){
        Main.Zakupy zakupy = new Main.Zakupy();
        String result = zakupy.Hello;
        assertEquals(result,"Witaj w zakupach online!");
    }
}
